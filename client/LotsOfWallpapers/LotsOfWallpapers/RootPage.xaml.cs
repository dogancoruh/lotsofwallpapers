﻿using Xamarin.Forms;

namespace LotsOfWallpapers
{
	public partial class LotsOfWallpapersPage : MasterDetailPage
	{
		public LotsOfWallpapersPage()
		{
			InitializeComponent();

			Master = new SideMenuPage();
			Detail = new MainPage();
		}
	}
}
