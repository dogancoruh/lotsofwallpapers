﻿using LotsOfWallpapers.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LotsOfWallpapers.Controllers
{
    public class MobileServiceController : Controller
    {
        // GET: MobileService
        public ActionResult Categories()
        {
            using (var dbContext = new DBEntities())
            {
                var categories = (from table in dbContext.WallpaperCategories
                                  orderby table.Order, table.Exclusive
                                  select table).ToList();

                var categories_ = new ArrayList();

                foreach (var category in categories)
                {
                    var category_ = new
                    {
                        id = category.Id,
                        name = category.Name,
                        description = category.Description,
                        exclusive = category.Exclusive == true
                    };

                    categories_.Add(category_);
                }

                var result = new
                {
                    resultCode = 0,
                    resultData = categories_
                };

                return Json(result, "application/json",  Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
        }
    }
}