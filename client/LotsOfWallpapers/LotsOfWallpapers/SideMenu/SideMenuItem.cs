﻿using System;
namespace LotsOfWallpapers
{
	public class SideMenuItem
	{
		public string Name { get; set; }
		public string Title { get; set; }

		public SideMenuItem(string name, string title)
		{
			Name = name;
			Title = title;
		}
	}
}
